package danafix.doku.test.controller;

import danafix.doku.test.config.HttpUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.StringWriter;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@RestController
public class RestApi {
    @Autowired
    HttpServletRequest request;
    @Autowired
    HttpUtils httpUtils;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @RequestMapping(value = {"/inquiry"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> deduction() {
        JSONObject jsonResult = new JSONObject();

        log.info("Received request from: "+request.getRemoteHost()+" ( "+ request.getRemoteAddr()+" )");
        try {
            int i=0;
            Enumeration<String> headers = request.getHeaderNames();
            while(headers.hasMoreElements()) {
                i++;
                String header = headers.nextElement();
                log.info("HEADER["+i+"] "+header+" = "+request.getHeader(header));
            }

            Enumeration<String> params = request.getParameterNames();
            i=0;
            //String url = "http://testiddoku.mfi-ap.asia/0/rest/DokuService/Inquiry";

            String url = "http://testiddoku.mfi-ap.asia/0/rest/DokuService/Inquiry";

            List<NameValuePair> postParams = new ArrayList<NameValuePair>();
            CloseableHttpClient client = httpUtils.getHttpClient();
            HttpPost httpPost = new HttpPost(url);
            while(params.hasMoreElements()) {
                i++;
                String param = params.nextElement();
                log.info("PARAM["+i+"] "+param+" = "+request.getParameter(param));
                postParams.add(new BasicNameValuePair(param, request.getParameter(param)));

            }

            httpPost.setEntity(new UrlEncodedFormEntity(postParams));
            CloseableHttpResponse response = client.execute(httpPost);

            HttpEntity entity = response.getEntity();
            byte[] bytes = EntityUtils.toByteArray(entity);
            String respText = new String(bytes);

            log.info("Response: "+respText);

            HttpHeaders respHeaders = new HttpHeaders();
            return new ResponseEntity<String>(respText, respHeaders, HttpStatus.OK);
        }
        catch (Exception e) {
            log.error("Error while proxying: "+e);
        }

        return defaultReturn(jsonResult);
    }


    private ResponseEntity defaultReturn(JSONObject jsonResult) {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }
}
